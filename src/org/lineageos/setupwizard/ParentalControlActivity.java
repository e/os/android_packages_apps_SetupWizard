/*
 * Copyright (C) 2024 MURENA SAS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.lineageos.setupwizard;

import androidx.activity.result.ActivityResult;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.os.Bundle;
import androidx.browser.customtabs.CustomTabsIntent;

public class ParentalControlActivity extends SubBaseActivity {

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setNextText(R.string.skip);
    }

    @Override
    protected void onStartSubactivity() {
        setNextAllowed(true);
        findViewById(R.id.activate_parental_control).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchParentalControl();
            }
        });
        findViewById(R.id.more_info_textview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String docUrl = getString(R.string.more_info_text);
                try {
                    CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().setShowTitle(true).build();
                    customTabsIntent.launchUrl(getApplicationContext(), Uri.parse(docUrl));
                } catch (Exception e) {
                    // Fallback to default browser
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(docUrl));
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void onSubactivityResult(ActivityResult activityResult) {
        Intent data = activityResult.getData();
        if (data != null && data.getBooleanExtra("onBackPressed", false)) {
            onStartSubactivity();
            return;
        // Check for back press from Parental control activity
        } else if(activityResult.getResultCode() == RESULT_OK) {
            onNavigateNext();
        }

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.setup_parental_control;
    }

    @Override
    protected int getTitleResId() {
        return R.string.parental_control_setup_title;
    }

    @Override
    protected int getIconResId() {
        return R.drawable.ic_account_manager_screen;
    }

    private void launchParentalControl() {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("foundation.e.parentalcontrol", "foundation.e.parentalcontrol.MainActivity"));
        startSubactivity(intent);
    }
}
