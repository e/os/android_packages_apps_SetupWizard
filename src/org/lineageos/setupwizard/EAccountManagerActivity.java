/*
 * Copyright (C) 2018-2021 E FOUNDATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.lineageos.setupwizard;

import static org.lineageos.setupwizard.SetupWizardApp.ACCOUNT_TYPE_E;

import androidx.activity.result.ActivityResult;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.os.Bundle;
import static org.lineageos.setupwizard.SetupWizardApp.LOGV;

import org.lineageos.setupwizard.util.SetupWizardUtils;

public class EAccountManagerActivity extends SubBaseActivity {

    public static final String TAG = EAccountManagerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (LOGV) {
            logActivityState("onCreate savedInstanceState=" + savedInstanceState);
        }
        setNextText(R.string.skip);
    }

    @Override
    protected void onStartSubactivity() {
        setNextAllowed(true);
        findViewById(R.id.sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAccountManagerSetup();
            }
        });
        findViewById(R.id.create_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SetupWizardApp.ACTION_CREATE_E_ACCOUNT);
                startSubactivity(intent);
            }
        });
    }

    @Override
    protected void onSubactivityResult(ActivityResult activityResult) {
        Intent data = activityResult.getData();
        if (data != null && data.getBooleanExtra("onBackPressed", false)) {
            onStartSubactivity();
            return;
        // Check for back press from AccountManager activity
        } else if (isEAccountAvailable()) {
            Log.v(TAG, "An E account is already set up; skipping EAccountManagerActivity");
            nextAction(RESULT_OK);
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.setup_e_account_manager;
    }

    @Override
    protected int getTitleResId() {
        return R.string.e_account_manager_setup_title;
    }

    @Override
    protected int getIconResId() {
        return R.drawable.ic_account_manager_screen;
    }

    private void launchAccountManagerSetup() {
        Intent intent = new Intent(Settings.ACTION_ADD_ACCOUNT)
                .putExtra(Settings.EXTRA_ACCOUNT_TYPES, new String[]{ACCOUNT_TYPE_E});
        startSubactivity(intent);
    }

    private boolean isEAccountAvailable() {
        Account[] allAccounts = getSystemService(AccountManager.class).getAccounts();
        for (Account account : allAccounts) {
            if (account.type.equals(ACCOUNT_TYPE_E)) {
                return true;
            }
        }
        return false;
    }

}
